package com.yuhj.cateyemovie.utils;


import com.yuhj.cateyemovie.R;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * @name LoadingAinm
 * @Descripation 分别在activity和Fragment中加载动画<br>
 * @author 禹慧军
 * @date 2014-10-30
 * @version 1.0
 */
public class LoadingAinm {
	 public static void ininLodingView(View view){
		  ImageView loadingImageView=(ImageView)view.findViewById(R.id.loading_imageView);
		  TextView loadingTextView=(TextView)view.findViewById(R.id.lodiing_text);
	    	loadingImageView.setBackgroundResource(R.anim.loading);
	        final AnimationDrawable animationDrawable = (AnimationDrawable)loadingImageView.getBackground();
	    	loadingImageView.post(new Runnable() {
	    	    @Override
	    	        public void run()  {
	    	            animationDrawable.start();
	    	        }
	    	});
	    }
	 public static void ininLoding(Activity activity){
		  ImageView loadingImageView=(ImageView)activity.findViewById(R.id.loading_imageView);
		  TextView loadingTextView=(TextView)activity.findViewById(R.id.lodiing_text);
	    	loadingImageView.setBackgroundResource(R.anim.loading);
	        final AnimationDrawable animationDrawable = (AnimationDrawable)loadingImageView.getBackground();
	    	loadingImageView.post(new Runnable() {
	    	    @Override
	    	        public void run()  {
	    	            animationDrawable.start();
	    	        }
	    	});
	    }
}

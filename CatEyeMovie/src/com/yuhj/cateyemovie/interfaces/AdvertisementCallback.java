package com.yuhj.cateyemovie.interfaces;

import java.util.ArrayList;

import com.yuhj.cateyemovie.bean.Advertisement;
import com.yuhj.cateyemovie.bean.PlayingMovieList;

public interface AdvertisementCallback{
	
	void getresult(ArrayList<Advertisement> result);
}
package com.yuhj.cateyemovie.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.TextView;

/**
 * @name ImagePageAdapter
 * @Descripation 图片的Viewpager<br>
 * @author 禹慧军
 * @date 2014-11-5
 * @version 1.0
 */
public class ImagePageAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments;
    private Context context;
   
  
	public ImagePageAdapter(FragmentManager fm,ArrayList<Fragment> fragments) {
		super(fm);
		this.context=context;
		this.fragments=fragments;
	}

	@Override
	public Fragment getItem(int arg0) {
		return fragments.get(arg0);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

}

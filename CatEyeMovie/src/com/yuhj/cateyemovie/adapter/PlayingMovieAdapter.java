package com.yuhj.cateyemovie.adapter;

import java.util.ArrayList;

import com.baidu.android.bbalbs.common.a.c;
import com.baidu.platform.comapi.map.v;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.bean.PlayingMovieList;
import com.yuhj.cateyemovie.utils.ImageCache;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class PlayingMovieAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<PlayingMovieList> data;
	private LruCache<String, Bitmap> lruCache;
	private BitmapDisplayConfig displayConfig;
	private BitmapUtils bitmapUtils;

	public PlayingMovieAdapter(Context context) {
		this.context = context;
		// bitmapUtils =new BitmapUtils(context);
		lruCache = ImageCache.GetLruCache(context);
		// displayConfig=new BitmapDisplayConfig();
		// Resources resources = context.getResources();
		// Drawable loading
		// =resources.getDrawable(R.drawable.bg_default_cat_gray);
		// displayConfig.setLoadingDrawable(loading);
	}

	public void BindData(ArrayList<PlayingMovieList> data) {

		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHoder viewHoder = null;
		if (convertView == null) {
			viewHoder = new ViewHoder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.playing_item, null);
			viewHoder.conten = (TextView) convertView
					.findViewById(R.id.movie_information);
			viewHoder.img = (ImageView) convertView
					.findViewById(R.id.movie_image);
			viewHoder.isnew = (ImageView) convertView
					.findViewById(R.id.movie_tag2);
			viewHoder.level = (TextView) convertView
					.findViewById(R.id.movie_level);
			viewHoder.name = (TextView) convertView
					.findViewById(R.id.movie_title);
			viewHoder.summary = (TextView) convertView
					.findViewById(R.id.movie_summary);
			viewHoder.tag = (ImageView) convertView
					.findViewById(R.id.movie_tag1);
			viewHoder.buy = (Button) convertView
					.findViewById(R.id.txt_buy_movie_ticket);
			convertView.setTag(viewHoder);
		} else {
			viewHoder = (ViewHoder) convertView.getTag();
		}
		PlayingMovieList plaingMovieList = data.get(position);
		viewHoder.conten.setText(plaingMovieList.getContent());
		viewHoder.level.setText(plaingMovieList.getLevel() + "分");
		viewHoder.name.setText(plaingMovieList.getName());
		viewHoder.summary.setText(plaingMovieList.getSummmary());
		String tag1 = plaingMovieList.getTag();
		if (tag1.contains("IMAX 3D")) {
			viewHoder.tag.setImageResource(R.drawable.ic_3d_imax);
		} else if (tag1.contains("2D")) {
			viewHoder.tag.setImageResource(R.drawable.ic_2d_imax);
		} else if (tag1.contains("3D")) {
			viewHoder.tag.setImageResource(R.drawable.ic_3d);
		}

		final Button button =viewHoder.buy;
		if (plaingMovieList.isNew()) {
			button.setText("购票");
			button.setTextColor(context.getResources().getColor(R.color.btn_buy_color));
			button.setBackgroundResource(R.drawable.btn_buy_bg);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					
					Toast.makeText(context, "购票", 1).show();

				}
			});
		} else if (!plaingMovieList.isNew()) {
			button.setText("预售");
			button.setBackgroundResource(R.drawable.btn_will_buy_bg);
			button.setTextColor(context.getResources().getColor(R.color.btn_will_buy_color));
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					
					Toast.makeText(context, "预售", 1).show();

				}
			});
		}

		// bitmapUtils.display(viewHoder.img,plaingMovieList.getImg(),
		// displayConfig);
		viewHoder.img.setTag(plaingMovieList.getImg());
		viewHoder.img.setImageResource(R.drawable.bg_default_cat_gray);
		new ImageCache(context, lruCache, viewHoder.img,
				plaingMovieList.getImg(), "CatEyeMovie", 90, 120);
		return convertView;
	}

	private class ViewHoder {
		TextView name;
		TextView summary;
		TextView conten;
		TextView level;
		ImageView img;
		ImageView tag;
		ImageView isnew;
		Button buy;
	}

}

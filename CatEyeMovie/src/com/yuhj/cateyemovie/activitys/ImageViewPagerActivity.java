package com.yuhj.cateyemovie.activitys;

import java.util.ArrayList;
import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.R.layout;
import com.yuhj.cateyemovie.adapter.ImagePageAdapter;
import com.yuhj.cateyemovie.clients.ClientApi;
import com.yuhj.cateyemovie.effect.CubeTransformer;
import com.yuhj.cateyemovie.effect.ZoomOutPageTransformer;
import com.yuhj.cateyemovie.fragment.MovieFragment;
import com.yuhj.cateyemovie.fragment.MovieImageFragment;
import com.yuhj.cateyemovie.interfaces.ImageCallback;
import com.yuhj.cateyemovie.widget.DepthPageTransformer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.Window;
import android.widget.TextView;

public class ImageViewPagerActivity extends FragmentActivity implements
		OnPageChangeListener {
	private ViewPager viewPager;
	private ImagePageAdapter adapter;
	private TextView title;
	private int position;
	private String movie_id ;
	private String url ;
	private ArrayList<Fragment> fragments = new ArrayList<Fragment>();

	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			if (msg == null) {

			} else {
				ArrayList<String> images = (ArrayList<String>) msg.obj;
				for (int i = 0; i < images.size(); i++) {
					fragments.add(new MovieImageFragment(images.get(i),
							getApplicationContext()));
				}
				title.setText(position+" / "+fragments.size());
				viewPager.setAdapter(adapter);
				viewPager.setCurrentItem(position);
			}

		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Intent intent =getIntent();
		setContentView(R.layout.activity_image_view_pager);
		movie_id = intent.getStringExtra("id");
		position=intent.getIntExtra("position", 1);
		url ="http://api.mobile.meituan.com/dianying/movie/"
		+ movie_id
		+ "/photos.json?utm_campaign=AmovieBmovieC110189035512576D-1&movieBundleVersion=100&utm_source=wandoujia&utm_medium=android&utm_term=100&utm_content=352706060172190&ci=1&uuid=A1A230538BAA987DFF805E95D3B8D53EAB6025EC580F2C6C56CF303DD5BB5A2E";
		adapter = new ImagePageAdapter(getSupportFragmentManager(), fragments);
		viewPager = (ViewPager) findViewById(R.id.image_viewpager);
		viewPager.setOnPageChangeListener(this);
		title = (TextView) findViewById(R.id.movie_name);
		new Thread(new Runnable() {

			@Override
			public void run() {
				ClientApi.getBigImage(url, new ImageCallback() {

					@Override
					public void getresult(ArrayList<String> result) {
						Message message = Message.obtain();
						message.obj = result;
						handler.sendMessage(message);
					}
				});
			}
		}).start();
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		//viewPager.setPageTransformer(true, new DepthPageTransformer());
		viewPager.setPageTransformer(true, new CubeTransformer());
		//viewPager.setTransitionEffect(TransitionEffect.RotateDown);
	}

	@Override
	public void onPageSelected(int arg0) {
		title.setText(arg0+" / "+fragments.size());
	}

}

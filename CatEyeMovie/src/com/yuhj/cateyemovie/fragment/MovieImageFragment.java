package com.yuhj.cateyemovie.fragment;

import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.utils.ImageCache;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

/**
 * @name ImageFragment
 * @Descripation <br>
 *               图片Fragment
 * @author 禹慧军
 * @date 2014-11-1
 * @version 1.0
 */
@SuppressLint("ValidFragment")
public class MovieImageFragment extends Fragment {
	private String url;
	private LruCache<String, Bitmap> lruCache;
	private Context context;

	@SuppressLint("ValidFragment")
	public MovieImageFragment(String url, Context context) {
		this.url = url;
		lruCache = ImageCache.GetLruCache(context);
		this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view =inflater.inflate(R.layout.movie_image, container,false);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageViewmovie);
		imageView.setImageResource(R.drawable.bg_default_cat_gray);
		new ImageCache(context, lruCache, imageView, url, "CatEyeMovie", 2000,
				1000);
		return view;
	}

}
